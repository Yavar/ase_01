#!/usr/bin/env python
# coding: utf-8

# #### Dtata Base


from fnmatch import filter
import os
import operator as op
import warnings
from collections import OrderedDict
from os import path


import pandas as pd

from ase.atoms import Atoms
from ase.calculators.singlepoint import (SinglePointDFTCalculator,
                                         SinglePointKPoint)
from ase.calculators.calculator import kpts2ndarray, kpts2sizeandoffsets
from ase.dft.kpoints import kpoint_convert
from ase.constraints import FixAtoms, FixCartesian
from ase.data import chemical_symbols, atomic_numbers
from ase.units import create_units
from ase.utils import iofunction
from ase.visualize import view

from numpy import *
from ase.db import connect
import glob 
from ase import Atoms
from ase.io import read, write
import sqlite3

import os

#############################################################################



##########    ###############################################################


## Avoiding adding duplicate entries





#####################################     add a cif 2 db and check exsistance


def addcif2dbunique(dbfile,ciffile):
    
    """
    

    Parameters
    ----------
    dbfile : string
        database file stored in path or new d.
    ciffile : string
        The name of cif file .

    Returns 
    -------
    k  :int
        if something added to database k=1 and if nothing k=0

    """
    
    
    # Count number of entity
    
    # Read cif into a data
    newcif=read(ciffile)

    dbc = connect(dbfile)

    newnumb=1
    k=1

    # CREAT  NEW DATABASE
    if os.path.exists(dbfile):
        # Extract rows in database
        rownumb=dbc.count()
        for j in range(rownumb):
            row = read(str(dbfile)+"@id="+str(j+1))[0]
            if ( (str(row.symbols)==str(newcif.symbols)) & ((row.cell[:,:]==newcif.cell[:,:]).all()) & (len(row)==len(newcif)) ):
  #              print("Same cell and number of atoms")
                if ( (row.positions==newcif.positions).all() ):
 #                   print("SAME STRUCTURES FOUND")
                    k=k-1
                else:
                    newnumb+=1
            else:
                newnumb+=1
            
        if (k == 1):
            dbc.write(newcif)

   # OPEN exsisting database and add new cif
    else:
        dbc.write(newcif)


    return k
        
        
   
    
#############      OPEN A PATH DIR AND FIND ALL CIF FILES AND ADDTHEM TO @dbfile
def addcifgroup2db(dbfile, cifpathdir):
    """
    

    Parameters
    ----------
    dbfile : string
        databasename.
    cifpathdir : string
        path of cif directory file.

    Returns
    -------
    nncif : int
        number of new cif files.

    """
    ciflist=filter(os.listdir(cifpathdir), '*.[Cc][Ii][Ff]')
    nncif=0
    for ciffile in ciflist:
        address=cifpathdir+"/"+ciffile
        k=addcif2dbunique(dbfile, address)
        
        nncif=nncif+k
    
    print(str(nncif)+"  cif files added to "+str(dbfile)) 
    
    
    return nncif
    

    
    
    
#########################################################   ADD a calculator to a cif file





def ADDcols2asedb(asedbfile, columlist):
    """
    Parameters
    ----------
    asedbfile : string
        The name of database.
    columlist : list
        list of new columns.

    Returns
    -------
    None.

    """
    
    
    # EDIT columns to add several col
    
    dbc=sqlite3.connect(asedbfile)
    
    cur=dbc.cursor()
    
    for colm in columlist:
        addColumn = "ALTER TABLE systems ADD COLUMN "+colm+ " varchar(32)"
        cur.execute(addColumn)
    cur.close()
    
    
    return



#########  UPDATE VALUE in ad column of database

def updateSqliteTable(dbfile, colname, id,  value):
    """
    Parameters
    ----------
    dbfile : string
            The name of database file.
    colname : string
        The name of column for update.
    id : int
        row id in database.
    value : TEXT
        value to update your database.

    Returns
    -------
    None.

    """
    try:
        sqliteConnection = sqlite3.connect(dbfile,  timeout=10)
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sql_update_query = "Update systems set "+colname+" = ? where id = ?"
        data = ( value, id)
        cursor.execute(sql_update_query, data)
        sqliteConnection.commit()
        print("Record Updated successfully")
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to update sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("The sqlite connection is closed")


####################################################################################


##  RETURN LIST OF FORMULA FOR ALL ROWS
def dbformula(dbfile):
    """
    

    Parameters
    ----------
    dbfile : string
        The name of database.

    Returns
    -------
    formulalist : list
        list of all formula for all compunds.

    """
    formulalist=[]
    test=connect(dbfile)
    n=test.count()
    for i in range (n):
        # get 
        a=test.get(id=i+1)
        formul=a.formula
        formulalist.append(formul)
    return formulalist



### !!!  no result

def element_basedsearch(dbfile, elementstring):
    """
    

    Parameters
    ----------
    dbfile : string
        THE NAME OF DATABASE.
    elementstring : string
        list of searched elemets separted by a space. for example 'Zr O'.

    Returns
    -------
    searchdic : TYPE
        DESCRIPTION.

    """
    
    
    elementlist=elementstring.split()
    formlalistofdb=dbformula(dbfile)
    test=connect(dbfile)
    searchdic={}
    i=0
    for formul in formlalistofdb:
        i+=1
        if all([val in formul  for val in elementlist ]):
            a=test.get(id=i)
            symb=a.formula
            searchdic.update({symb:i})
   
         
    return searchdic
        
       
  
    



##############  


##########   !!!  if db is ase db then db.get(id=1)  returns ase.db.row.AtomsRow while db.get_atoms(id=1) returns ase atoms
######  There are some diffs between these two OBJECTS   //  row object can be easily converted to the atoms objetct with .toatoms()
## for example one can use 



###     !!! To Do list
#   WE NEED @ TYPE OF CALCULATOR  ASE-SUP and ASEUNSUP




# def asesupcalc()
    
    
    

#    filecif = zeros(len(files), dtype = void )
#    for j in range(len(files)):
#        filecif = read(files[j])
#        dbconnection.write(filecif)
    
#     row = db.get(id=j+1)
#     for key in row:
#         print('{0:22}: {1}'.format(key, row[key])) 
    
#     #e2 = row.energy
#     #filecif.get_potential_energy()
#     #row = db.get(id=key)
#     row.cell
#     row.positions
#     ###get_ipython().run_line_magic('save', 'session%s.py -r 1-1000'%j)
   
# from ase.db import connect

# db = connect('DataBase.db')
# for row in db.select():
#     atoms = row.toatoms()
# #    view(atoms)
#     #print(atoms)


# # ### Add additional data



# a = read("DataBase.db@id=10")









